package com.rxlab.cloudservicegateway.config;


import com.rxlab.cloudservicegateway.filter.AuthenticationFilter;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.circuitbreaker.resilience4j.ReactiveResilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.gateway.filter.ratelimit.RedisRateLimiter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

import java.time.Duration;

/***
 * @author Rohit Patidar
 */
@Configuration
public class GatewayConfig {

    @Autowired
    AuthenticationFilter authenticationFilter;

    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder routeLocatorBuilder) {
        return routeLocatorBuilder.routes()
                .route(p -> p
                        .path("/api/order/**")
                        .filters(f -> {
                            f.filter(authenticationFilter)
                                    .requestRateLimiter().configure(c -> c.setRateLimiter(redisRateLimiter()))
                                    .circuitBreaker(c -> c.setName("orderCB").setFallbackUri("/orderFallBack"));
                            return f;
                        })
                        .uri("lb://CLOUD-ORDER-SERVICE")
                )
                .route(p -> p
                        .path("/api/payment/**")
                        .filters(f -> {
                            f.filter(authenticationFilter)
                                    .requestRateLimiter().configure(c -> c.setRateLimiter(redisRateLimiter()))
                                    .circuitBreaker(c -> c.setName("paymentCB").setFallbackUri("/paymentFallback"));
                            return f;
                        })
                        .uri("lb://CLOUD-PAYMENT-SERVICE")
                )
                .route(p -> p
                        .path("/api/oauth2/**", "/api/auth/**", "/api/user/**", "/login/oauth2/**")
                        .filters(f -> {
                            f.filter(authenticationFilter)
                                    .requestRateLimiter().configure(c -> c.setRateLimiter(redisRateLimiter()))
                                    .circuitBreaker(c -> c.setName("authCB").setFallbackUri("/authFallback"));
                            return f;
                        })
                        .uri("lb://CLOUD-AUTHENTICATION-SERVICE")
                )
                .route(p -> p
                        .path("/api/product/**")
                        .filters(f -> {
                            f.filter(authenticationFilter)
                                    .requestRateLimiter().configure(c -> c.setRateLimiter(redisRateLimiter()))
                                    .circuitBreaker(c -> c.setName("productCB").setFallbackUri("/productFallback"));
                            return f;
                        })
                        .uri("lb://CLOUD-PRODUCT-SERVICE")
                )
                .route(p -> p
                        .path("/api/cart/**")
                        .filters(f -> {
                            f.filter(authenticationFilter)
                                    .requestRateLimiter().configure(c -> c.setRateLimiter(redisRateLimiter()))
                                    .circuitBreaker(c -> c.setName("cartCB").setFallbackUri("/cartFallback"));
                            return f;
                        })
                        .uri("lb://CLOUD-CART-SERVICE")
                )
                .route(p -> p
                        .path("/api/email/**")
                        .filters(f -> {
                            f.filter(authenticationFilter)
                                    .requestRateLimiter().configure(c -> c.setRateLimiter(redisRateLimiter()))
                                    .circuitBreaker(c -> c.setName("notificationCB").setFallbackUri("/notificationFallback"));
                            return f;
                        })
                        .uri("lb://CLOUD-NOTIFICATION-SERVICE")
                )
                .build();
    }

    @Bean("config")
    public Customizer<ReactiveResilience4JCircuitBreakerFactory> defaultCustomizer() {

        return factory -> factory.configureDefault(id -> new Resilience4JConfigBuilder(id)
                .circuitBreakerConfig(CircuitBreakerConfig
                        .ofDefaults())
                .timeLimiterConfig(TimeLimiterConfig.custom()
                        .timeoutDuration(Duration.ofSeconds(2))
                        .build())
//                        .custom()
//                        .minimumNumberOfCalls(1)
//                        .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
//                        .slidingWindowSize(1)
//                        .waitDurationInOpenState(Duration.ofMillis(5000))
//                        .slowCallDurationThreshold(Duration.ofMinutes(1))
//                        .slowCallRateThreshold(1f)
//                        .failureRateThreshold(1f)
//                        .build())
                .build());
    }

    @Bean
    RedisRateLimiter redisRateLimiter() {
        return new RedisRateLimiter(10, 20);
    }

    @Bean
    KeyResolver userKeyResolver() {
        return exchange -> Mono.just("1");
    }

}
