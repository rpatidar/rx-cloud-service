package com.rxlab.cloudservicegateway.controller;

import com.rxlab.cloudcodelib.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/***
 * @author Rohit Patidar
 */

@RestController
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FallbackController {

    @RequestMapping("/orderFallBack")
    public Response<?> orderServiceFallBack() {
        return Response.badRequest().setResBody("Order Service is taking too long to respond or is down. Please try again later");
    }

    @RequestMapping("/paymentFallback")
    public Response<?> paymentServiceFallBack() {
        return Response.badRequest().setResBody("Payment Service is taking too long to respond or is down. Please try again later");
    }

    @RequestMapping("/authFallback")
    public Response<?> authServiceFallBack() {
        return Response.badRequest().setResBody("Authentication Service is taking too long to respond or is down. Please try again later");
    }

    @RequestMapping("/productFallback")
    public Response<?> productServiceFallBack() {
        return Response.badRequest().setResBody("Product Service is taking too long to respond or is down. Please try again later");
    }
    @RequestMapping("/cartFallback")
    public Response<?> cartServiceFallBack() {
        return Response.badRequest().setResBody("Cart Service is taking too long to respond or is down. Please try again later");
    }
    @RequestMapping("/notificationFallback")
    public Response<?> notificationFallback() {
        return Response.badRequest().setResBody("Notification Service is taking too long to respond or is down. Please try again later");
    }
}
