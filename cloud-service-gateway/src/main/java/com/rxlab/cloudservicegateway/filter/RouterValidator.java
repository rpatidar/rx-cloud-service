package com.rxlab.cloudservicegateway.filter;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;

/***
 * @author Rohit Patidar on 15/04/21
 */
@Component
public class RouterValidator {

    public static final List<String> openApiEndpoints = List.of(
            "/api/oauth2/",
            "/api/auth/",
            "/login/oauth2/"
    );

    public Predicate<ServerHttpRequest> isSecured =
            request -> openApiEndpoints
                    .stream()
                    .noneMatch(uri -> request.getURI().getPath().contains(uri));
}
