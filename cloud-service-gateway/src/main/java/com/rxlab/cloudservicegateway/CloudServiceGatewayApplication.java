package com.rxlab.cloudservicegateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/***
 * @author Rohit Patidar
 */

@SpringBootApplication
@EnableEurekaClient
public class CloudServiceGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudServiceGatewayApplication.class, args);
	}

}
