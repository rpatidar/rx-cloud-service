package com.rxlab.cloudauthenticationservice.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rxlab.cloudauthenticationservice.entity.User;
import com.rxlab.cloudauthenticationservice.filter.JwtFilter;
import com.rxlab.cloudauthenticationservice.services.CustomOidcUserService;
import com.rxlab.cloudauthenticationservice.services.CustomUserDetailsService;
import com.rxlab.cloudauthenticationservice.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/***
 * @author Rohit Patidar
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ObjectMapper mapper;
//    private final TokenStore tokenStore;
//    private final TokenFilter tokenFilter;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private JwtFilter jwtFilter;

    @Autowired
    private CustomUserDetailsService customUserDetailService;

    @Autowired
    private CustomOidcUserService customOidcUserService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers().frameOptions().disable().and()
                .csrf().disable().cors()
                .and().authorizeRequests()
                .antMatchers("/oauth2/**", "/login**", "/api/auth/list-provider", "/h2-console/**", "/actuator/httptrace", "/actuator/httptrace/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .disable()
                .oauth2Login()
                .authorizationEndpoint()
                .baseUri("/api/oauth2/authorize-client")
                .authorizationRequestRepository(new InMemoryRequestRepository())
                .and()
                .successHandler(this::successHandler)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(this::authenticationEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .logout(customHandler -> customHandler.addLogoutHandler(this::logout)
                        .logoutUrl("/api/user/logout")
                        .logoutSuccessHandler(this::onLogoutSuccess));

        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

    private void logout(HttpServletRequest request, HttpServletResponse response,
                        Authentication authentication) {
        // You can process token here
        System.out.println("Auth token is - " + request.getHeader("Authorization"));
    }

    void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
                         Authentication authentication) {
        // this code is just sending the 200 ok response and preventing redirect
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedOrigins(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    private void successHandler(HttpServletRequest request,
                                HttpServletResponse response, Authentication authentication) throws IOException {
        String name = authentication.getName();
        String emailId = authentication.getName().contains("@") ? authentication.getName() : authentication.getName() + "@example.com";

        OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) authentication;
        try {
            switch (oAuth2AuthenticationToken.getAuthorizedClientRegistrationId()) {
                case "google":
                case "gitlab":
                    name = oAuth2AuthenticationToken.getPrincipal().getAttribute("name").toString();
                    emailId = oAuth2AuthenticationToken.getPrincipal().getAttribute("email").toString();
                    break;
//                case "github":
//                    name = oAuth2AuthenticationToken.getPrincipal().getAttribute("name").toString();
//                    emailId = authentication.getName().contains("@") ? authentication.getName() : authentication.getName() + "@example.com";;
//                    break;
                case "twitch":
//                    name = oAuth2AuthenticationToken.getPrincipal().getAttribute("name").toString();
                    emailId = oAuth2AuthenticationToken.getPrincipal().getAttribute("email").toString();
                    break;
            }

//            name = oAuth2AuthenticationToken.getPrincipal().getAttribute("name").toString();
//            emailId = oAuth2AuthenticationToken.getPrincipal().getAttribute("email").toString();

            System.out.println("OIDC user");
            System.out.println(oAuth2AuthenticationToken.getAuthorizedClientRegistrationId());
            System.out.println(oAuth2AuthenticationToken.getAuthorities());
            System.out.println(oAuth2AuthenticationToken.getPrincipal().getAttributes());

        } catch (Exception exception) {
            System.out.println("conversion error");
        }
        System.out.println("Success Handler");
        System.out.println(authentication);
        User user = new User();
        user.setEmailId(emailId)
                .setPassword(UUID.randomUUID().toString())
                .setName(name)
                .setRegisteredVia(oAuth2AuthenticationToken.getAuthorizedClientRegistrationId())
                .setAuthenticated(authentication.isAuthenticated());

        user = customUserDetailService.saveUser(user);

        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", user.getId());
        claims.put("name", user.getName());
        String token = jwtUtil.generateToken(user.getEmailId(), claims);
        response.getWriter().write(
                mapper.writeValueAsString(Collections.singletonMap("accessToken", token))
        );
    }

    private void authenticationEntryPoint(HttpServletRequest request, HttpServletResponse response,
                                          AuthenticationException authException) throws IOException {

        System.out.println("EntryPoint");
        System.out.println(authException);

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write(mapper.writeValueAsString(Collections.singletonMap("error", "Unauthenticated")));
    }
}
