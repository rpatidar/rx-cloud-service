package com.rxlab.cloudauthenticationservice.repository;

import com.rxlab.cloudauthenticationservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/***
 * @author Rohit Patidar
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmailId(String email);
}
