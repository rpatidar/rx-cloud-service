package com.rxlab.cloudauthenticationservice.controller;

import com.rxlab.cloudauthenticationservice.model.AuthProviders;
import com.rxlab.cloudcodelib.response.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/***
 * @author Rohit Patidar
 */

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    @GetMapping("/list-provider")
    public Response<?> getAuthProviders() {
        List<AuthProviders> authProviders = new ArrayList<>();
        authProviders.add( new AuthProviders(
                "Google",
                "google",
                "/assets/images/google.png",
                "/api/oauth2/authorize-client/google",
                "/api/login/oauth2/token/google"
        ));

        authProviders.add( new AuthProviders(
                "Twitch",
                "twitch",
                "/assets/images/twitch.png",
                "/api/oauth2/authorize-client/twitch",
                "/api/login/oauth2/token/twitch"
        ));

        authProviders.add( new AuthProviders(
                "Git Hub",
                "github",
                "/assets/images/github.png",
                "/api/oauth2/authorize-client/github",
                "/api/login/oauth2/token/github"
        ));

        authProviders.add( new AuthProviders(
                "Git Lab",
                "gitlab",
                "/assets/images/gitlab.png",
                "/api/oauth2/authorize-client/gitlab",
                "/api/login/oauth2/token/gitlab"
        ));
        return Response.ok().setResBody(authProviders);
    }

}
