package com.rxlab.cloudauthenticationservice.controller;

import com.rxlab.cloudauthenticationservice.services.CustomUserDetailsService;
import com.rxlab.cloudcodelib.response.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Collections;

/***
 * @author Rohit Patidar
 */

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private CustomUserDetailsService userService;

    @GetMapping("/me-via-oauth2")
    public Response<?> userMeOAuth2(@AuthenticationPrincipal OAuth2User principal) {
        return Response.ok().setResBody(Collections.singletonMap("name", principal.getName()));
    }

    @GetMapping("/me")
    public Response<?> user(Authentication authentication, Principal principal) {
        return Response.ok().setResBody(Collections.singletonMap("name", userService.getUser(principal.getName()).getName()));
    }
}
