package com.rxlab.cloudauthenticationservice.services;

import com.rxlab.cloudauthenticationservice.entity.User;
import com.rxlab.cloudauthenticationservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

/***
 * @author Rohit Patidar
 */

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmailId(email);
        return new org.springframework.security.core.userdetails.User(user.getEmailId(), user.getPassword(), Collections.emptyList());
    }

    public User saveUser(User user){
        User savedUser = userRepository.findByEmailId(user.getEmailId());
        if(savedUser == null){
            savedUser = userRepository.save(user);
        }
        return savedUser;
    }

    public User getUser(String emailId){
        return userRepository.findByEmailId(emailId);
    }
}
