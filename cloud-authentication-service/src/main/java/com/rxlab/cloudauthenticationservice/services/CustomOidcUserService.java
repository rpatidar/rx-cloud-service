package com.rxlab.cloudauthenticationservice.services;

import com.rxlab.cloudauthenticationservice.entity.User;
import com.rxlab.cloudauthenticationservice.model.OidcUserInfo;
import com.rxlab.cloudauthenticationservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

/***
 * @author Rohit Patidar on 16/04/21
 */
@Service
public class CustomOidcUserService extends OidcUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
        System.out.println("loadUser");
        System.out.println(userRequest.getAccessToken().getTokenValue());
        System.out.println(userRequest.getIdToken().getClaims());
        OidcUser oidcUser = super.loadUser(userRequest);

        try {
            return processOidcUser(userRequest, oidcUser);
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OidcUser processOidcUser(OidcUserRequest userRequest, OidcUser oidcUser) {
        OidcUserInfo oidcUserInfo = new OidcUserInfo(oidcUser.getAttributes());
        System.out.println("processOidcUser");
        System.out.println(oidcUser);
        System.out.println(oidcUserInfo.getEmail());
        System.out.println(oidcUser.getClaims());
        System.out.println(oidcUser.getAccessTokenHash());
        System.out.println(oidcUser.getIdToken());
        System.out.println(oidcUser.getAccessTokenHash());
        System.out.println(oidcUser.getEmail());
        System.out.println(oidcUser.getWebsite());
        return oidcUser;
    }
}
