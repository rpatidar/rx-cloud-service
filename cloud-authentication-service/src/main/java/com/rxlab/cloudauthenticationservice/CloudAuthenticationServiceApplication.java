package com.rxlab.cloudauthenticationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/***
 * @author Rohit Patidar
 */

@SpringBootApplication
@EnableEurekaClient
public class CloudAuthenticationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudAuthenticationServiceApplication.class, args);
	}

}
