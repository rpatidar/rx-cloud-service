package com.rxlab.cloudauthenticationservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * @author Rohit Patidar
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthProviders {
    private String name;
    private String label;
    private String imagePath;
    private String authUrl;
    private String loginUrl;
}
