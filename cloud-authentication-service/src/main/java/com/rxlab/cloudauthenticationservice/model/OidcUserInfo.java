package com.rxlab.cloudauthenticationservice.model;

import java.util.Map;

/***
 * @author Rohit Patidar on 16/04/21
 */
public class OidcUserInfo {

    private Map<String, Object> attributes;

    public OidcUserInfo(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return (String) attributes.get("sub");
    }

    public String getName() {
        return (String) attributes.get("name");
    }

    public String getEmail() {
        return (String) attributes.get("email");
    }
}
