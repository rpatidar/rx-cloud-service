package com.rxlab.cloudcartservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CloudCartServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudCartServiceApplication.class, args);
	}

}
