package com.rxlab.cloudcartservice.service;

import com.rxlab.cloudcartservice.entity.Cart;
import com.rxlab.cloudcartservice.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/***
 * @author Rohit Patidar
 */

@Service
public class CartServiceImpl implements CartService{

    @Autowired
    private CartRepository cartRepository;

    @Override
    public long getCartItemsCount(long userId) {
        return cartRepository.countByUserId(userId);
    }

    @Override
    public List<Cart> getCartItems(long userId) {
        Optional<List<Cart>> cartItems = cartRepository.findByUserId(userId);
        return cartItems.orElse(Collections.emptyList());
    }

    @Override
    public long addItem(Cart cart) {
        cartRepository.save(cart);
        return getCartItemsCount(cart.getUserId());
    }

    @Override
    @Transactional
    public void emptyCart(long userId) {
        cartRepository.deleteByUserId(userId);
    }
}
