package com.rxlab.cloudcartservice.service;

import com.rxlab.cloudcartservice.entity.Cart;

import java.util.List;

/***
 * @author Rohit Patidar
 */

public interface CartService {
    /**
     * Get Cart Item count
     * @param userId
     * @return Item Count
     */
    long getCartItemsCount(long userId);

    /**
     * Get Cart Items
     * @param userId
     * @return List of Cart Items
     */
    List<Cart> getCartItems(long userId);

    /**
     * Add Item in Cart
     *
     * @param cart
     * @return Item Id
     */
    long addItem(Cart cart);

    /**
     * Empty Cart
     * @param userId
     * @return Void
     */
    void emptyCart(long userId);
}
