package com.rxlab.cloudcartservice.repository;

import com.rxlab.cloudcartservice.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/***
 * @author Rohit Patidar
 */
public interface CartRepository extends JpaRepository<Cart, Long> {
    long countByUserId(Long userId);

    Optional<List<Cart>> findByUserId(Long userId);

    void deleteByUserId(long userId);
}
