package com.rxlab.cloudcartservice.controller;

import com.rxlab.cloudcartservice.entity.Cart;
import com.rxlab.cloudcartservice.service.CartService;
import com.rxlab.cloudcodelib.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/***
 * @author Rohit Patidar
 */

@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping("/item-count")
    public Response<?> getCartItemsCount(@RequestHeader("userId") String userId){
        return Response.ok().setResBody(cartService.getCartItemsCount(Long.parseLong(userId)));
    }

    @GetMapping("/items")
    public Response<?> getCartItems(@RequestHeader("userId") String userId){
        return Response.ok().setResBody(cartService.getCartItems(Long.parseLong(userId)));
    }

    @PostMapping("/add")
    public Response<?> addItem(@RequestBody Cart cart, @RequestHeader("userId") String userId){
        cart.setUserId(Long.parseLong(userId));
        return Response.ok().setResBody(cartService.addItem(cart));
    }

    @DeleteMapping("/items")
    public Response<?> deleteItem(@RequestHeader("userId") String userId){
        cartService.emptyCart(Long.parseLong(userId));
        return Response.ok();
    }
}
