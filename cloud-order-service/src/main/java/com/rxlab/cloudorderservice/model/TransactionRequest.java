package com.rxlab.cloudorderservice.model;

import com.rxlab.cloudorderservice.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/***
 * @author Rohit Patidar
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {

    private List<Order> orderList;
    private Card card;
    private Payment payment;
}
