package com.rxlab.cloudorderservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/***
 * @author Rohit Patidar
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Payment {
    private long paymentId;
    private String paymentStatus;
    private String transactionId;
    private long orderId;
    private double amount;
    private long userId;
}

