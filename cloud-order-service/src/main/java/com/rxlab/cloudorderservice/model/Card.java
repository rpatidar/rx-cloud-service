package com.rxlab.cloudorderservice.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/***
 * @author Rohit Patidar
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Card {
    private String fullName;
    private long cardNumber;
    private String cardEx;
    private int cardCvt;
    private double amount;
    private long userId;
}
