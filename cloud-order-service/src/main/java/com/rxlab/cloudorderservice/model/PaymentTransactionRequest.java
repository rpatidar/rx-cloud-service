package com.rxlab.cloudorderservice.model;

import com.rxlab.cloudorderservice.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/***
 * @author Rohit Patidar
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PaymentTransactionRequest {
    private Card card;
    private Payment payment;
}
