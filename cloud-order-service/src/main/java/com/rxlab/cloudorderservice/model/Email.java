package com.rxlab.cloudorderservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/***
 * @author Rohit Patidar
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Email {
    private String to;
    private String recipientName;
    private String subject;
    private String text;
}
