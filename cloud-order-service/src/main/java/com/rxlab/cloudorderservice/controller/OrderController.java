package com.rxlab.cloudorderservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rxlab.cloudcodelib.response.Response;
import com.rxlab.cloudorderservice.model.TransactionRequest;
import com.rxlab.cloudorderservice.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/***
 * @author Rohit Patidar
 */

@RestController
@RequestMapping("/api/order/")
public class OrderController {

    @Autowired
    private OrderServiceImpl orderService;

    private Random random = new Random();

    @PostMapping("place")
    public Response<?> placeOrder(@RequestBody TransactionRequest request, @RequestHeader("userId") String userId) {
        try {
            Boolean res = orderService.saveOrder(request, Long.parseLong(userId));
            if(res){
                return Response.ok().setResBody("Payment processed and order placed");
            }else{
                //Failure random response list
                List<String> resList = new ArrayList<>();
                resList.add("There is a failure in payment, please try again.");
                resList.add("Insufficient amount");
                resList.add("Bank server is not responding, please try again later.");
                resList.add("Transaction failure, please try again.");

                int index = random.nextInt(resList.size());
                return Response.exception().setResBody(resList.get(index));
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
