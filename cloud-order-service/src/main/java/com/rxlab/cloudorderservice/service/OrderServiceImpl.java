package com.rxlab.cloudorderservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rxlab.cloudorderservice.entity.Order;
import com.rxlab.cloudorderservice.model.*;
import com.rxlab.cloudorderservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/***
 * @author Rohit Patidar
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository repository;

    @Autowired
    private RestService restService;

    @Override
    public Boolean saveOrder(TransactionRequest request, long userId) throws JsonProcessingException {
        Instant instant = Instant.now();
        long orderId = instant.toEpochMilli();

        List<Order> orderList = request.getOrderList();
//        orderList = orderList.stream().peek(item -> {
//            item.setUserId(tmpUserId)
//                    .setOrderId(orderId);
//        }).collect(Collectors.toList());
        orderList = orderList.stream().map(item -> {
            item.setUserId(userId)
                    .setOrderId(orderId);
            return item;
        }).collect(Collectors.toList());

        Payment payment = request.getPayment();
        payment.setOrderId(orderId).setAmount(request.getCard().getAmount()).setUserId(userId);
        //rest call
        repository.saveAll(orderList);

        PaymentTransactionRequest payTransReq = new PaymentTransactionRequest();
        request.getCard().setUserId(userId);
        payTransReq.setPayment(payment).setCard(request.getCard());

        boolean response = true;
        try {
            /* Post call to Payment microservice */
            Payment paymentResponse = restService.getPaymentDetails(payTransReq);
            response = paymentResponse.getPaymentStatus().equals("success");

            if(response){
                Email email = new Email(
                        "rpatidar@isystango.com",
                        "Rohit",
                        "Rx Code eCommerce Order Placed",
                        "Dear Customer, Your order has been placed."
                );
                /*Post call to Email microservice */
                restService.sendEmail(email);
            }
        } catch (Exception exp) {
            response = false;
        }
        return response;
    }
}
