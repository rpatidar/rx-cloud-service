package com.rxlab.cloudorderservice.service;

import com.rxlab.cloudorderservice.model.Email;
import com.rxlab.cloudorderservice.model.Payment;
import com.rxlab.cloudorderservice.model.PaymentTransactionRequest;

import java.util.concurrent.CompletableFuture;

/***
 * @author Rohit Patidar
 */
public interface RestService {
    /**
     * Get Payment Details
     * @param paymentTransactionRequest
     * @return Payment Object
     */
    Payment getPaymentDetails(PaymentTransactionRequest paymentTransactionRequest);

    /**
     * Send Email
     * @param email
     * @return Void
     */
    void sendEmail(Email email) throws InterruptedException;


}
