package com.rxlab.cloudorderservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rxlab.cloudorderservice.model.TransactionRequest;

/***
 * @author Rohit Patidar
 */
public interface OrderService {

    /**
     * Save Order
     *
     * @param request
     * @return Boolean
     */
    Boolean saveOrder(TransactionRequest request, long userId) throws JsonProcessingException;
}
