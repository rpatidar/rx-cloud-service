package com.rxlab.cloudorderservice.service;

import com.rxlab.cloudorderservice.model.Email;
import com.rxlab.cloudorderservice.model.Payment;
import com.rxlab.cloudorderservice.model.PaymentTransactionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

/***
 * @author Rohit Patidar
 */
@Service
@RefreshScope
public class RestServiceImpl implements RestService {

    @Autowired
    @Lazy
    private RestTemplate template;

    @Value("${microservice.payment-service.endpoints.endpoint.uri}")
    private String PAYMENT_ENDPOINT_URL;

    @Value("${microservice.notification-service.endpoints.endpoint.uri}")
    private String NOTIFICATION_ENDPOINT_URL;

    @Override
    public Payment getPaymentDetails(PaymentTransactionRequest payTransReq) {
        return template.postForObject(PAYMENT_ENDPOINT_URL, payTransReq, Payment.class);
    }

    @Async("emailSendTaskExecutor")
    @Override
    public void sendEmail(Email email) throws InterruptedException {
        template.postForObject(NOTIFICATION_ENDPOINT_URL, email, Email.class);
        // Artificial delay of 1s for demonstration purposes
        Thread.sleep(1000L);
    }
}
