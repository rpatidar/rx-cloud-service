package com.rxlab.cloudorderservice.repository;

import com.rxlab.cloudorderservice.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/***
 * @author Rohit Patidar
 */

public interface OrderRepository extends JpaRepository<Order,Integer> {
}

