package com.rxlab.cloudproductservice.service;

import com.rxlab.cloudproductservice.entity.Product;
import com.rxlab.cloudproductservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/***
 * @author Rohit Patidar
 */

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAllProductList() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getProductList(List<Long> productIds) {
        return productRepository.findByProductIdIn(productIds);
    }
}
