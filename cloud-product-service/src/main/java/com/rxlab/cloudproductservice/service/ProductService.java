package com.rxlab.cloudproductservice.service;

import com.rxlab.cloudproductservice.entity.Product;

import java.util.List;
import java.util.Optional;

/***
 * @author Rohit Patidar
 */
public interface ProductService {
    /**
     * Get All Product List
     *
     *
     * @return List<Product>
     */
    List<Product> getAllProductList();

    /**
     * Get Product List by Ids
     *
     * @param productIds
     * @return List<Product>
     */
    List<Product> getProductList(List<Long> productIds);
}
