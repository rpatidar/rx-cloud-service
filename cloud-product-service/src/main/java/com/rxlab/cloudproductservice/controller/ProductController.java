package com.rxlab.cloudproductservice.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rxlab.cloudcodelib.response.Response;
import com.rxlab.cloudproductservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

/***
 * @author Rohit Patidar
 */

@RestController
@RequestMapping("/api/product")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    public Response<?> getProducts() {
        return Response.ok().setResBody(productService.getAllProductList());
    }

    @PostMapping("/list")
    public Response<?> getProductsById(@RequestBody List<Long> productIds) {
        return Response.ok().setResBody(productService.getProductList(productIds));
    }
}
