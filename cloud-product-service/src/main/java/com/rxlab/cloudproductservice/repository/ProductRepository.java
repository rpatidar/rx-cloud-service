package com.rxlab.cloudproductservice.repository;

import com.rxlab.cloudproductservice.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/***
 * @author Rohit Patidar
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByProductIdIn(List<Long> productIds);
}
