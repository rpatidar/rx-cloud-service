package com.rxlab.cloudpaymentservice.repository;

import com.rxlab.cloudpaymentservice.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

/***
 * @author Rohit Patidar
 */
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
    Payment findByOrderId(int orderId);
}
