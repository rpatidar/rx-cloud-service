package com.rxlab.cloudpaymentservice.repository;

import com.rxlab.cloudpaymentservice.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/***
 * @author Rohit Patidar
 */
public interface CardRepository extends JpaRepository<Card, Long> {
    List<Card> findAllByUserId(long userId);
}
