package com.rxlab.cloudpaymentservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rxlab.cloudcodelib.response.Response;
import com.rxlab.cloudpaymentservice.entity.Card;
import com.rxlab.cloudpaymentservice.entity.Payment;
import com.rxlab.cloudpaymentservice.model.CardResponse;
import com.rxlab.cloudpaymentservice.model.TransactionRequest;
import com.rxlab.cloudpaymentservice.service.CardService;
import com.rxlab.cloudpaymentservice.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

/***
 * @author Rohit Patidar
 */
@RestController
@RequestMapping("/api/payment/")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CardService cardService;

    @PostMapping("do-payment")
    public Payment doPayment(@RequestBody TransactionRequest request) {
        try {
            String cardId = cardService.saveCard(request.getCard());
            request.getPayment().setCardId(cardId);
            return paymentService.doPayment(request.getPayment());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("saved-cards")
    public Response<?> savedCards(@RequestHeader("userId") String userId) {
        List<Card> cards = cardService.getSavedCards(Long.parseLong(userId));
        List<CardResponse> cardResults = cards.stream()
                .map(c -> new CardResponse(c.getCardNumber(), c.getFullName(), c.getCardEx()))
                .collect(Collectors.toList());
        return Response.ok().setResBody(cardResults);
    }

    @GetMapping("/{orderId}")
    public Payment findPaymentHistoryByOrderId(@PathVariable int orderId) {
        return paymentService.findPaymentHistoryByOrderId(orderId);
    }
}

