package com.rxlab.cloudpaymentservice.service;

import com.rxlab.cloudcodelib.utils.Md5Utils;
import com.rxlab.cloudpaymentservice.entity.Card;
import com.rxlab.cloudpaymentservice.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

/***
 * @author Rohit Patidar
 */

@Service
public class CardServiceImpl implements CardService{

    @Autowired
    private CardRepository cardRepository;

    @Override
    public String saveCard(Card card) throws NoSuchAlgorithmException {
        String key = Md5Utils.getMd5(card.getCardNumber() + card.getCardEx() + card.getCardCvt() + card.getUserId());
        card.setId(key);
        cardRepository.save(card);
        return key;
    }

    @Override
    public List<Card> getSavedCards(long userId) {
        return cardRepository.findAllByUserId(userId);
    }
}
