package com.rxlab.cloudpaymentservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rxlab.cloudpaymentservice.entity.Payment;

/***
 * @author Rohit Patidar
 */
public interface PaymentService {
    /**
     * Do Payment
     *
     * @param payment
     * @return Payment
     */
    Payment doPayment(Payment payment) throws JsonProcessingException;

    /**
     * Get Payment History
     *
     * @param orderId
     * @return Payment
     */
    Payment findPaymentHistoryByOrderId(int orderId);
}
