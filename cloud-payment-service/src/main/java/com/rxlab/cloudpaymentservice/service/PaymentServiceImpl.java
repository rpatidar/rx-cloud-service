package com.rxlab.cloudpaymentservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rxlab.cloudpaymentservice.entity.Payment;
import com.rxlab.cloudpaymentservice.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.UUID;

/***
 * @author Rohit Patidar
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository repository;

    @Override
    public Payment doPayment(Payment payment) {
        payment.setPaymentStatus(paymentProcessing());
        payment.setTransactionId(UUID.randomUUID().toString());
        return repository.save(payment);
    }

    @Override
    public Payment findPaymentHistoryByOrderId(int orderId) {
        return repository.findByOrderId(orderId);
    }

    public String paymentProcessing() {
        //api should be 3rd party payment gateway.
        return new Random().nextBoolean() ? "success" : "false";
    }
}
