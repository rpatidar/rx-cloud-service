package com.rxlab.cloudpaymentservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rxlab.cloudpaymentservice.entity.Card;

import java.security.NoSuchAlgorithmException;
import java.util.List;

/***
 * @author Rohit Patidar
 */
public interface CardService {
    /**
     * Save Card
     *
     * @param card
     * @return Card key
     */
    String saveCard(Card card) throws JsonProcessingException, NoSuchAlgorithmException;

    /**
     * Saved Card List
     *
     * @param userId
     * @return List of cards
     */
    List<Card> getSavedCards(long userId);
}
