package com.rxlab.cloudpaymentservice.model;

import com.rxlab.cloudpaymentservice.entity.Card;
import com.rxlab.cloudpaymentservice.entity.Payment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * @author Rohit Patidar
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {
    Payment payment;
    Card card;
}
