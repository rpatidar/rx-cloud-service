package com.rxlab.cloudpaymentservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * @author Rohit Patidar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardResponse {
    private long cardNumber;
    private String fullName;
    private String cardEx;
}
