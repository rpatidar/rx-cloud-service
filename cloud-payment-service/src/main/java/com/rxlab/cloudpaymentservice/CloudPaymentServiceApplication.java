package com.rxlab.cloudpaymentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CloudPaymentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudPaymentServiceApplication.class, args);
	}

}
