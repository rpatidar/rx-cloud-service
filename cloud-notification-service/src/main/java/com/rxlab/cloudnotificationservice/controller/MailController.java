package com.rxlab.cloudnotificationservice.controller;

import com.rxlab.cloudnotificationservice.model.Email;
import com.rxlab.cloudnotificationservice.service.EmailService;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/***
 * @author Rohit Patidar
 */

@RestController
@RequestMapping("/api/email/")
public class MailController {

    @Autowired
    public EmailService emailService;

    @PostMapping("send")
    public String createMail(@RequestBody @Valid Email mailObject) {
        emailService.sendSimpleMessage(mailObject.getTo(),
                mailObject.getSubject(), mailObject.getText());

        return "emails";
    }

    @GetMapping("send")
    public String test(){
        return "Test";
    }
}
