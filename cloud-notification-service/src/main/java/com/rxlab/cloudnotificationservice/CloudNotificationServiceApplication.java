package com.rxlab.cloudnotificationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/***
 * @author Rohit Patidar
 */

@SpringBootApplication
@EnableEurekaClient
public class CloudNotificationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudNotificationServiceApplication.class, args);
	}

}
