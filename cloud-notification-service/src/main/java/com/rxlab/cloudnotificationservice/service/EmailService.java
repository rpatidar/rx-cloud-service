package com.rxlab.cloudnotificationservice.service;

/***
 * @author Rohit Patidar
 */
public interface EmailService {

    /**
     * Get Send Simple Email
     * @param to
     * @param subject
     * @param text
     * @return Void
     */
    void sendSimpleMessage(String to, String subject, String text);
}
